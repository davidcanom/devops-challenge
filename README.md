# DevOps Challenge

Prueba diseñada para ver tus habilidades en el mundo DevOps. Se evaluará las herramientas fundamentales que utilizamos tales como docker, Nomad/Consul, CI/CD y Terraform. 

¡Te deseamos mucho éxito!


## Tareas
* Dockerizar la aplicacion node
* Crear un pipleine para construir la imagen.
* Crear un Nomad job para desplegar la imagen en Nomad
* Crear un archivo Terraform para lanzar una instancia en EC2 con estas caracteristicas: 
  * Instancia **t3a.nano**
  * Disco con **20GB**
  * Acceso SSH a la IP **11.22.33.44**
  * Imagen base **Amazon Linux 2**
* Documentar las acciones realizadas 
  
**Importante**    
> Documentar correctamente el proceso es muy importante para asegurar que el equipo pueda continuar con las tareas de ser necesario. 

## Bonus
* Imagen docker pequeña
* Ejecutar con un usuario distinto a root
* Que el pipeline ejecute los tests antes de hacer build
* Usar Forever o PM2

# Procedimiento

1. crear un archivo dockerfile
2. agregar el comando `FROM` para elegir la imagen base (node) y definir una imagen base de bajo size a.k.a slim resultando `node:14-slim` el mejor candidato
3. agregar un comando `RUN` para crear el usuario `devops` que permitira correr el proyecto.
4. copiar escalonadamente los archivos para reducir tiempos de build por repeticion en capas.
5. identificar el usuario con el que se grabara la imagen docker gracias al comando `USER`
6. especificar el comando a usar por defecto `npm run start`
7. crear un archivo `docker-compose.yml` para correr el servicio (esto debido a que tengo windows + WSL + docker y me es mas facil trabajar en modo developer asi)
8. crear un archivo `main.tf` para poder desarrollar los requerimientos de la instancia en terraform.
9. crear un bloque `aws_instance` con las propiedades correspondientes que se solicitaron (instance_type,ami). NOTA: consegui la AMI directamente de la consola de amazon por razones de sencilles. Es posible crear un bloque data para consultar la imagen mas actual al momento de ejecucion de terraform plan
10. crear un bloque `aws_security_group` para poder definir las reglas de ingress que permiten ek trafico SSH en el puerto 22 y desde la ip `11.22.33.44/32`
11. crear un bloque `aws_ebs_volume` para crear un disco 20GB
12. crear un bloque `aws_volume_attachment` para enlazar el disco a la instancia ec2. NOTA: existen muchos otros archivos de terraform que faltan para que sea finalmente ejecutable. Me he enfocado solo en los archivos solicitados para ser optimo con el tiempo.
13. prosegui a crear un archivo `.gitlab-ci.yml` que es un pipeline como codigo para gitlab-ci, tambien es muy similar a los github actions, bitbucket pipelines, droneio. Principales plataformas de CI/CD que aprovecha el mundo conteinarizado.
14. defini los stages necesarios para hacer build y el test del proyecto. build usando imagen docker. Test haciendo uso de una magen node:14-slim para las pruebas. NOTA: mencionaron usar Forever o PM2 pero descartaria su uso ya que al ser una imagen docker es posible establecer los healthchecks que permitan que el container se reinicie, ello por buenas practicas de un despliegue.
15. defini el orden de los stages haciendo que las pruebas se ejecuten previo al build y todos los stages tienen definido que se ejecuten ante cualquier push en la rama master