FROM node:14-slim AS build
RUN useradd -ms /bin/bash devops
WORKDIR /app
COPY ./package*.json ./
RUN npm install
COPY ./ ./
USER devops
CMD ["npm","run","start"]