resource "aws_security_group" "allow_ssh" {
  name        = "allow_ssh"
  description = "Allow SSH inbound traffic"
  vpc_id      = "my_vpc_id"

  ingress = [
    {
      description = "TLS from VPC"
      from_port   = 22
      to_port     = 22
      protocol    = "tcp"
      cidr_blocks = ["11.22.33.44/32"]
    }
  ]

  egress = [
    {
      from_port        = 0
      to_port          = 0
      protocol         = "-1"
      cidr_blocks      = ["0.0.0.0/0"]
      ipv6_cidr_blocks = ["::/0"]
    }
  ]

  tags = {
    Name = "allow_ssh"
  }
}

resource "aws_ebs_volume" "disk1" {
  availability_zone = "us-west-2a"
  size              = 20

  tags = {
    Name = "Devops Challenge"
  }
}

resource "aws_instance" "vm1" {
  ami             = "ami-087c17d1fe0178315"
  instance_type   = "t3a.nano"
  key_name        = "my_key_name"
  security_groups = ["${aws_security_group.allow_ssh.id}"]

  tags = {
    Name = "Devops Challenge"
  }
}

resource "aws_volume_attachment" "vm1disk1" {
  device_name = "/dev/sdc"
  volume_id   = aws_ebs_volume.disk1.id
  instance_id = aws_instance.vm1.id
}